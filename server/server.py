#!/usr/bin/env python
# coding: utf-8

import pyjsonrpc
import subprocess
import threading 

import os

class RequestHandler(pyjsonrpc.HttpRequestHandler):
  def get(self):
    self.response.headers.add_header("Access-Control-Allow-Origin", "*")
    
  @pyjsonrpc.rpcmethod
  def run_bladerf(self):
    #bladerf = subprocess.Popen("./receiver", shell=True,
    #                             stdin=subprocess.PIPE, stdout=subprocess.PIPE,
    #                             stderr=subprocess.STDOUT,close_fds=False)
    
    p = subprocess.Popen(["./receiver"],
                         shell=True, 
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE)
    stdout_data, stderr_data = p.communicate()
    return stdout_data


# Threading HTTP-Server
http_server = pyjsonrpc.ThreadingHttpServer(
    server_address = ('localhost', 8080),
    RequestHandlerClass = RequestHandler
)

print "Starting HTTP server ..."
print "URL: http://localhost:8080"
http_server.serve_forever()
