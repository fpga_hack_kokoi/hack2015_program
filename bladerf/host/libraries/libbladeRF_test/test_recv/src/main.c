#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <libbladeRF.h>

#include "conversions.h"
#include "devcfg.h"

#define TX_FREQUENCY    422000000
#define TX_SAMPLERATE   2000000
#define TX_BANDWIDTH    2500000


struct app_params {
  struct devcfg dev_config;
  bool rx;
  bool tx;
  uint64_t iterations;
  uint64_t randval_seed;
  uint64_t randval_state;
};

int main(int argc, char *argv[])
{
    int status = 0;
    char *devstr = NULL;
    struct bladerf *dev;

    int16_t *rx_samples = NULL;
    struct app_params params;
    unsigned int i;


    // Read Device Params
    devcfg_init(&params.dev_config);


    // Open Device
    status = bladerf_open(&dev, devstr);
    if (status != 0) {
        fprintf(stderr, "Failed to open device: %s\n",
                bladerf_strerror(status));
        return 1;
    }
	//fq
   status = bladerf_set_frequency(dev, BLADERF_MODULE_TX, TX_FREQUENCY);
	if (status != 0) {
		fprintf(stderr, "Faield to set TX frequency: %s\n", bladerf_strerror(status));
		return 1;
	} 
	else {
		printf("TX frequency: %u Hz\n", TX_FREQUENCY);
	}

	//samp
	status = bladerf_set_sample_rate(dev, BLADERF_MODULE_RX, TX_SAMPLERATE, NULL);
	if (status != 0) {
		fprintf(stderr, "Failed to set TX sample rate: %s\n", bladerf_strerror(status));
		return 1;
	}
	else {
		printf("TX sample rate: %u sps\n", TX_SAMPLERATE);
	}


	status = bladerf_set_bandwidth(dev, BLADERF_MODULE_TX, TX_BANDWIDTH, NULL);
	if (status != 0) {
		fprintf(stderr, "Failed to set TX bandwidth: %s\n", bladerf_strerror(status));
		return 1;
	}
	else {
		printf("TX bandwidth: %u Hz\n", TX_BANDWIDTH);
	}
	printf("Running...rs\n");


    // Receive Signal
    rx_samples = calloc(params.dev_config.samples_per_buffer, 2 * sizeof(int16_t));
   status = devcfg_perform_sync_config(dev, BLADERF_MODULE_RX, BLADERF_FORMAT_SC16_Q11, &(params.dev_config), true);

    if (status != 0) {
      status = -1;
      return -1;
    }

    const unsigned int to_rx = params.dev_config.samples_per_buffer;


    while(true){
      status = bladerf_sync_rx(dev,
			       rx_samples, 
			       to_rx,
			       NULL,
			       params.dev_config.sync_timeout_ms);
	
      if (status != 0) {
	fprintf(stderr, "Failed to RX samples. :%s\n",
		bladerf_strerror(status)
	      );
	return 1;
      }
      
      for(i=0;i < to_rx;i=i+2){
	printf("%d, %d\n", rx_samples[i]);
      }
    }


    free(rx_samples);
    // Close Device
    bladerf_close(dev);
   return 0;



}
